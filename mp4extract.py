#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import shutil


# In[25]:


def delFolder(path):
    
    check = True
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith( '.mp4' ):
                print("Bu klasor(mp4 var) silinemez:" + path)
                check = False
    if(check):
        try:
            shutil.rmtree(path)
            print("Klasor Silindi: "+path)
        except OSError as e:
            print ("\nError SILME: %s.\n" % (e))


# In[26]:


def function(path):
    for root, dirs, files in os.walk(path):
        for sub in dirs:
            for main, dirs, files in os.walk(os.path.join(root, sub)):
                for file in files:
                    try:
                        if file.endswith( '.mp4' ):
                            shutil.move(os.path.join(main, file), root)
                            print('Kesildi: '+os.path.join(main, file))
                    except OSError as e:
                        print ("\nError KESME: %s.\n" % (e))
            print('silinmeye gonderilen: '+ os.path.join(root, sub))                
            delFolder(os.path.join(root, sub))


# In[24]:


print("Dosya yolunu gir: ")
path = str(input())
function(path)
print('\nCikmak icin tusa bas\n')
str(input())


# In[ ]:




